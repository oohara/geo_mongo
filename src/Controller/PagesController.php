<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
    public $uses = array();

/**
 * Displays a view
 *
 * @return void
 * @throws NotFoundException When the view file could not be found
 *   or MissingViewException in debug mode.
 */
    public function display() {
        $Area = ClassRegistry::init('Area');
        // var_dump($Area->find('first'));

        // 渋谷区役所
        $lat = 35.664035;
        $lon = 139.698212;

        // ラフォーレ原宿
        $lat = 35.66913;
        $lon = 139.705382;

        // // 札幌時計台ビル
        // $lat = 43.06311;
        // $lon = 141.35348;

        $mongo = $Area->getDataSource();
        $mongoCollectionObject = $mongo->getMongoCollection($Area);
        $query = array(
            'geo' => array(
                '$geoIntersects' => array(
                    '$geometry' => array(
                        'type' => 'Point',
                        'coordinates' => array($lon, $lat),
                    ),
                ),
            ),
        );
        $fields = array(
            'KEN_NAME',
            'GST_NAME',
            'CSS_NAME',
            'MOJI',
        );
        $cursor = $mongoCollectionObject->find($query, $fields);
        $array = iterator_to_array($cursor);
        debug($array);

        $address = '';
        foreach ($array as $id => $values) {
            foreach ($fields as $field) {
                $address .= Hash::get($values, $field);
            }
        }
        debug($address);


        $path = func_get_args();

        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        $page = $subpage = $title_for_layout = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        if (!empty($path[$count - 1])) {
            $title_for_layout = Inflector::humanize($path[$count - 1]);
        }
        $this->set(compact('page', 'subpage', 'title_for_layout'));

        try {
            $this->render(implode('/', $path));
        } catch (MissingViewException $e) {
            if (Configure::read('debug')) {
                throw $e;
            }
            throw new NotFoundException();
        }
    }
}
